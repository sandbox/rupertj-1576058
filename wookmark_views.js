(function ($) {

    Drupal.behaviors.wookmark_views = {
        attach: function (context, settings) {
            $('.wookmark-grid').each(function () {

                // Find the view wrapper, so we can get its name
                $view = $(this).closest('.view');

                var matches = $view.attr('class').match(/view-([a-z0-9_]+)/);
                var viewName = matches[1];

                $('li', $(this)).wookmark({
                    offset: Drupal.settings.wookmark_views[viewName].offset,
                    itemWidth: Drupal.settings.wookmark_views[viewName].itemWidth,
                    container: $(this)
                });
            });
        }
    };

})(jQuery);