<?php

class views_plugin_style_wookmark_grid extends views_plugin_style_list {

  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['offset'] = array('default' => 10);
    $options['item_width'] = array('default' => 180);

    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {

    parent::options_form($form, $form_state);

    // We don't want these items inherited from list:
    unset($form['type'], $form['class']);

    $form['offset'] = array(
      '#title' => t('Offset'),
      '#description' => t('The gap between items.'),
      '#type' => 'textfield',
      '#size' => '3',
      '#default_value' => $this->options['offset'],
    );

    $form['item_width'] = array(
      '#title' => t('Item Width'),
      '#description' => t('The width of each item.'),
      '#type' => 'textfield',
      '#size' => '3',
      '#default_value' => $this->options['item_width'],
    );
  }

}