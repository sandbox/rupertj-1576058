<?php
/**
 * @file wookmark-grid.tpl.php
 * @ingroup views_templates
 */
?>
<?php print $wrapper_prefix; ?>
<?php if (!empty($title)) : ?>
<h3><?php print $title; ?></h3>
<?php endif; ?>
<ul class="wookmark-grid">
<?php foreach ($rows as $id => $row): ?>
  <li><?php print $row; ?></li>
<?php endforeach; ?>
</ul>
<?php print $wrapper_suffix; ?>
